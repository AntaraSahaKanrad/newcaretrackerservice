﻿using Caretrackerservice.Data.Connections.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Data.Connections
{
    public class MongoDbSettings: IMongoDbSettings
    {
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
    }
}
