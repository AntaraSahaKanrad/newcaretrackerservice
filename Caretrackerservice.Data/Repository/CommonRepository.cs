﻿using Caretrackerservice.Core.Entity.BaseEntity;
using Caretrackerservice.Data.RepositoryManager.Contracts;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.Extensions.Logging;
namespace Caretrackerservice.Data.Repository
{
    public class CommonRepository<T> : ICommonRepository<T> where T : IDocument
    {
        private readonly IMongoRepository<T> _mongoRepository;
        private readonly ILogger<CommonRepository<T>> _logger;
        public CommonRepository(IMongoRepository<T> mongoRepository, ILogger<CommonRepository<T>> logger)
        {
            _mongoRepository = mongoRepository;
            _logger = logger;
        }
        public IEnumerable<T> GetCollectionData()
        {
            try { 
            return _mongoRepository.AsQueryable();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }
        public void CreateCollectionData(IEnumerable<T> collectionData)
        {
            try
            {

        
            _mongoRepository.InsertMany(collectionData.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }

        public void DeleteCollectionData(Expression<Func<T, bool>> filterExpression)
        {
            try { 
            _mongoRepository.DeleteOne(filterExpression);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }

        public void DeleteManyCollectionData(IEnumerable<T> collectionData)
        {
            try { 
            foreach (var items in collectionData) 
            { 
                Expression<Func<T, bool>> FilterByNameLength()
                {
                    return x => x._id == items._id;
                } 
                _mongoRepository.DeleteMany(FilterByNameLength());
            }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }

        }
        }

        
    }

