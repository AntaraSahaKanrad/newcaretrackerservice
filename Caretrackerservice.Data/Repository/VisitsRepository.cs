﻿using Caretrackerservice.Core.Entity;
using Caretrackerservice.Data.RepositoryManager.Contracts;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Caretrackerservice.Data.Repository
{
    public class VisitsRepository :  IVisitsRepository
    {
        private readonly IMongoRepository<VisitsEntity>  _mongoRepository;
        private readonly ILogger<CommonRepository<VisitsEntity>> _logger;
        public VisitsRepository(IMongoRepository<VisitsEntity> mongoRepository, ILogger<CommonRepository<VisitsEntity>> logger)
        {
            _mongoRepository = mongoRepository;
            _logger = logger;
        }
        public async Task<IEnumerable<VisitsEntity>> GetVisits()
        {
             
            var allvisits = _mongoRepository.AsQueryable();
            return allvisits;
        }       

        public async void CreateSingleVisit(VisitsEntity visitsEntity)
        {
            try
            { 
                await _mongoRepository.InsertOneAsync(visitsEntity);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }

        public async void CreateVisits(IEnumerable<VisitsEntity> visitsEntity)
        {
            try { 
                await _mongoRepository.InsertManyAsync(visitsEntity.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }



        public void ModifySingleVisit(VisitsEntity visitsEntity)
        {
            try
            {
                VisitsEntity visitsEntityResult = _mongoRepository.FilterBy(x => x.visitsBasicsEntity.ScheduleAppID == visitsEntity.visitsBasicsEntity.ScheduleAppID).FirstOrDefault();

                if (visitsEntityResult == null)
                {
                    CreateSingleVisit(visitsEntity);
                }
                else
                {

                    visitsEntity.visitsBasicsEntity = visitsEntityResult.visitsBasicsEntity;

                    _mongoRepository.ReplaceOneAsync(visitsEntity);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }

        public  void ModifyVisits(IEnumerable<VisitsEntity> visitsEntity)
        {
            try
            {                  

                if (visitsEntity.Any())
                {
                    foreach (var item in visitsEntity)
                    {
                        VisitsEntity visitsEntityResult = _mongoRepository.FilterBy(x=>x.visitsBasicsEntity.ScheduleAppID== item.visitsBasicsEntity.ScheduleAppID).FirstOrDefault();

                        if (visitsEntityResult == null)
                        {
                            CreateSingleVisit(item);
                        }
                        else
                        {

                            item.visitsBasicsEntity = visitsEntityResult.visitsBasicsEntity;

                            _mongoRepository.ReplaceOneAsync(item);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }

        public async void UpdateSingleVisitBasic(VisitsBasicsEntity visitsBasicsEntity, int cgtaskid)
        {
            try
            {
                VisitsEntity visitsEntity = new VisitsEntity();
                VisitsEntity visitsEntityResult = await _mongoRepository.FindOneAsync(filter => filter.visitsBasicsEntity.ScheduleAppID == cgtaskid);

                if (visitsEntityResult == null)
                {
                    visitsEntity.visitsBasicsEntity = visitsBasicsEntity;
                    CreateSingleVisit(visitsEntity);
                }
                else
                {
                    visitsEntityResult.visitsBasicsEntity = visitsBasicsEntity;
                    await _mongoRepository.ReplaceOneAsync(visitsEntity);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }
    }
}
