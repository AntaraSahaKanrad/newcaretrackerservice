﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text; 
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using Caretrackerservice.Data.Repository;
using Caretrackerservice.Data.Connections;
using Microsoft.Extensions.Options;
using Caretrackerservice.Data.Connections.Contracts;
using Caretrackerservice.Data.RepositoryManager.Contracts;
using Caretrackerservice.Data.RepositoryManager;

namespace Caretrackerservice.Data
{
    public static class ServiceCollectionExtensions
    { 
        public static IServiceCollection AddDataServiceCollection(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<MongoDbSettings>(x=> {
                x.ConnectionString= configuration.GetSection("MongoDbSettings:ConnectionString").Value;
                x.DatabaseName = configuration.GetSection("MongoDbSettings:DatabaseName").Value;
            });

            services.AddSingleton<IMongoDbSettings>(serviceProvider =>
                serviceProvider.GetRequiredService<IOptions<MongoDbSettings>>().Value
                );
            services.AddScoped(typeof(IMongoRepository<>), typeof(MongoRepository<>));

            services.AddScoped(typeof(ICommonRepository<>), typeof(CommonRepository<>));

            services.AddTransient<IVisitsRepository, VisitsRepository>(); 
            return services;
        }
    }
}
