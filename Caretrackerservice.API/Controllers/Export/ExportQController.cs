﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.Export
{
    [ApiController]
    public class ExportQController : ControllerBase
    {
        private readonly ILogger<ExportQController> _logger;

        public ExportQController(ILogger<ExportQController> logger)
        {
            _logger = logger;
        }

        [HttpGet]

        [Route("api/caretracker/info/exportQ/by_hha_vendor_method")]
        public IActionResult GetExportQ_By_HHA_Vendor_Method([FromQuery] int hha, [FromQuery] int vendorID, [FromQuery] string methodName)
        {

            _logger.LogInformation("----- Post EVVcaretrackerController");

            return Content("{\"HHAID\": \"1\",  \"HHAName\": \"AOC\",\"VendorName\": \"Tellus\",\"isPrimary\": \"0\"," +
                " \"ExportMethod\": \"Visits\",\"count \": \"100\",\"VendorUID \": \"1\"}");

        }

        [HttpGet]

        [Route("api/caretracker/info/exportQ/by_hha_vendor")]
        public IActionResult GetExportQ_By_HHA_Vendor([FromQuery] int hha, [FromQuery] int vendorID)
        {

            _logger.LogInformation("----- Post EVVcaretrackerController");

            return Content("{\"HHAID\": \"1\",  \"HHAName\": \"AOC\",\"VendorName\": \"Tellus\",\"isPrimary\": \"0\"," +
               " \"count \": \"10\",\"VendorUID \": \"1\"}");
        }

        [HttpGet]

        [Route("api/caretracker/info/exportQ/hha/hhaid/vendor/vendorid/exportMethod/methodName")]
        public IActionResult GetExportQItems([FromQuery] int hha, [FromQuery] int vendorid, [FromQuery] string methodName)
        {

            _logger.LogInformation("----- Post EVVcaretrackerController");

            return Content("{\"ClientName\": \"John\",  \"CaregiverName\": \"angela\"}");
        }
    }
}
