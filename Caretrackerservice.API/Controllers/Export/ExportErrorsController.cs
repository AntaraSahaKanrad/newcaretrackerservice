﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.Export
{
    [ApiController]
    public class ExportErrorsController : ControllerBase
    {
        private readonly ILogger<ExportErrorsController> _logger;

        public ExportErrorsController(ILogger<ExportErrorsController> logger)
        {
            _logger = logger;
        }


        [HttpPost]
        [Route("api/caretracker/info/download/primary/distinct_error")]
        public IActionResult GetExportErrors_Prim([FromQuery] int HHA, [FromQuery] string aggregator, [FromQuery] string DateFrom, [FromQuery] string DateTo)
        {
            _logger.LogInformation("----- Post ServiceHomeController");

            return Ok();
        }

        [HttpPost]
        [Route("api/caretracker/info/export/aggregator/distinct_error")]
        public IActionResult GetExportErrors_Aggregator([FromQuery] int HHA, [FromQuery] string aggregator, [FromQuery] string DateFrom, [FromQuery] string DateTo)
        {
            _logger.LogInformation("----- Post ServiceHomeController");

            return Ok();
        }
    }
}

