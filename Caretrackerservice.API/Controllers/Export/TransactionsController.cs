﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.Export
{

    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly ILogger<TransactionsController> _logger;

        public TransactionsController(ILogger<TransactionsController> logger)
        {
            _logger = logger;
        }


        [HttpPost]
        [Route("api/caretracker/info/exportHistory")]
        public IActionResult GetExportH([FromQuery] int BatchID, [FromQuery] string DateFrom, [FromQuery] string DateTo, [FromQuery] int HHA, [FromQuery] int vendorUID, [FromQuery] string ExportMethod)
        {
            _logger.LogInformation("----- Post ServiceHomeController");

            return Ok();
        }

        [HttpPost]
        [Route("api/caretracker/info/TransactionHistory")]
        public IActionResult GetTransactionHistory([FromQuery] string ObjectType, [FromQuery] int ObjectID, [FromQuery] int HHA)
        {
            _logger.LogInformation("----- Post ServiceHomeController");

            return Ok();
        }


        [Route("api/caretracker/download/primary/vendor/vendorUID/hha/hhaid")]
        [HttpPut]
        public IActionResult DownloadTransactions_Prim([FromQuery] int vendorUID, [FromQuery] int hhaId)
        {
            _logger.LogInformation("----- Put TransactionsController");

            return Ok();
        }

        [Route("api/caretracker/download/aggregator/vendor/vendorUID/hha/hhaid")]
        [HttpPut]
        public IActionResult DownloadTransactions_Aggregator([FromQuery] int vendorUID, [FromQuery] int hhaid)
        {
            _logger.LogInformation("----- Put TransactionsController");

            return Ok();
        }


        [HttpPost]
        [Route("api/caretracker/download/primary/aggregator_import")]
        public IActionResult DownloadTransactions_FromBinFile([FromQuery] string filename)
        {
            _logger.LogInformation("----- Post ServiceHomeController");

            return Ok();
        }
    }
}