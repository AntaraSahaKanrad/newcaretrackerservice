﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks; 
using Caretrackerservice.Services.Visits.Query;
using MediatR; 
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers
{
   
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {

        private IMediator _mediator; 

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IMediator Mediator)
        {
            _logger = logger;
            _mediator = Mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {

                var ret = await _mediator.Send(new GetVisitsQuery());
                return Ok(ret);
            }catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                    return NotFound(ex); ;
            }

}
    }
}
