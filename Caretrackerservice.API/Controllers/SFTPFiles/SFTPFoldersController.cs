﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.SFTPFiles
{
    [ApiController]
    public class SFTPFoldersController : ControllerBase
    {
        private readonly ILogger<SFTPFoldersController> _logger;

        public SFTPFoldersController(ILogger<SFTPFoldersController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        [Route("api/caretracker/sftp/info/folders/vendor/vendorUID/hha/hhaid/branch/branchid")]
        public IActionResult GetSFTPFolders_Vendor_HHA_Branch([FromQuery] int vendorUID, [FromQuery] int hhaid, [FromQuery] int branchid)
        {

            _logger.LogInformation("----- Post EVVPublishController");

            return Content("{\"foldername\": \"SFTP\"}");
        }


        [Route("api/caretracker/sftp/download/folders/vendor/vendorUID/hha/hhaid/branch/branchid")]
        [HttpPut]
        public IActionResult DownloadSFTPFolders_Vendor_HHA_Branch([FromQuery] int vendorUID, [FromQuery] int hhaid, [FromQuery] int branchid)
        {
            _logger.LogInformation("----- Put DownloadSFTPFolders_Vendor_HHA_Branch");

            return Ok();
        }
    }
}