﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.SFTPFiles
{

    [ApiController]
    public class SFTPFilesController : ControllerBase
    {
        private readonly ILogger<SFTPFilesController> _logger;

        public SFTPFilesController(ILogger<SFTPFilesController> logger)
        {
            _logger = logger;
        }




        [Route("api/caretracker/sftp/download/folder")]
        [HttpPut]
        public IActionResult DownloadSFTPFiles_Folder([FromQuery] string foldername)
        {
            _logger.LogInformation("----- Put SFTPFoldersController");

            return Ok();
        }

        [HttpGet]

        [Route("api/caretracker/sftp/info/folder/viewfiles")]
        public IActionResult GetSFTPFiles_Folder()
        {

            _logger.LogInformation("----- Post SFTPFoldersController");

            return Content("{\"foldername\": \"SFTP\"}");
        }

        [HttpGet]

        [Route("api/caretracker/sftp/info/folder/viewfile")]
        public IActionResult GetSFTPFolders_Vendor_HHA_Branch()
        {

            _logger.LogInformation("----- Post SFTPFoldersController");

            return Content("{\"filename\": \"SFTP\"}");
        }
    }
}
