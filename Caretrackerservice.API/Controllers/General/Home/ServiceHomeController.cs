﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.General.Home
{

    [ApiController]
    public class ServiceHomeController : ControllerBase
    {
        private readonly ILogger<ServiceHomeController> _logger;

        public ServiceHomeController(ILogger<ServiceHomeController> logger)
        {
            _logger = logger;
        }




        [HttpPost]
        [Route("api/caretracker/callback")]
        public IActionResult ReceiveVendorResponse([FromBody] int HHA)
        {
            _logger.LogInformation("----- Post ServiceHomeController");

            return Ok();
        }
    }
}
