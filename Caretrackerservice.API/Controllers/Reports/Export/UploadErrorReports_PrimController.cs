﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.Reports.Export
{

    [ApiController]
    public class UploadErrorReports_PrimController : ControllerBase
    {
        private readonly ILogger<UploadErrorReports_PrimController> _logger;

        public UploadErrorReports_PrimController(ILogger<UploadErrorReports_PrimController> logger)
        {
            _logger = logger;
        }




        [HttpPost]
        [Route("api/caretracker/report/primary/upload_error")]
        public IActionResult GetUploadErrorsReport_Aggregator([FromQuery] int HHAID, [FromQuery] int vendorID)
        {
            _logger.LogInformation("----- Post DownloadErrorReportsController_PrimController");

            return Ok();
        }
    }
}