﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.Reports.Export
{

    [ApiController]
    public class SubErrorReports_AggregatorController : ControllerBase
    {
        private readonly ILogger<SubErrorReports_AggregatorController> _logger;

        public SubErrorReports_AggregatorController(ILogger<SubErrorReports_AggregatorController> logger)
        {
            _logger = logger;
        }




        [HttpPost]
        [Route("api/caretracker/report/aggregator/submission_error")]
        public IActionResult GetSubErrorsReport_Aggregator([FromQuery] int HHAID, [FromQuery] int vendorID)
        {
            _logger.LogInformation("----- Post DownloadErrorReportsController_PrimController");

            return Ok();
        }
    }
}
