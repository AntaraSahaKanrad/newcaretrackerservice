﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.Reports.Export
{
    [ApiController]
    public class DownloadErrorReports_PrimController : ControllerBase
    {
        private readonly ILogger<DownloadErrorReports_PrimController> _logger;

        public DownloadErrorReports_PrimController(ILogger<DownloadErrorReports_PrimController> logger)
        {
            _logger = logger;
        }




        [HttpPost]
        [Route("api/caretracker/report/primary/download_error")]
        public IActionResult GetDownloadErrorsReport_Prim([FromQuery] int HHAID, [FromQuery] int vendorID)
        {
            _logger.LogInformation("----- Post DownloadErrorReportsController_PrimController");

            return Ok();
        }


    }
}
