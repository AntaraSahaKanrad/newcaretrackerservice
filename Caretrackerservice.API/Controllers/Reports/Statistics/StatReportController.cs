﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Caretrackerservice.API.Controllers.Reports.Statistics
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatReportController : ControllerBase
    {
        private readonly ILogger<StatReportController> _logger;

        public StatReportController(ILogger<StatReportController> logger)
        {
            _logger = logger;
        }


        [HttpGet]

        [Route("api/caretracker/report/aggregator/statistics")]
        public IActionResult GetStatReport([FromQuery] int hha, [FromQuery] int user, [FromQuery] string Locations,
            [FromQuery] int SecondaryEvvVendor, [FromQuery] string StartDate, [FromQuery] string EndDate, [FromQuery] int context, [FromQuery] bool IncludeEvvErrors, [FromQuery] int purpose)
        {

            _logger.LogInformation("----- Post EVVcaretrackerController");

            return Content("{\"ClientName\": \"John\",  \"CaregiverName\": \"angela\"}");
        }
    }
}