﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("EvvVendorMaster")]
    public class EvvVendorMasterEntity : Document
    {
        public int EvvVendorMasterID { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string VendorName { get; set; }
        public string VendorCode { get; set; }
        public bool isPrimary { get; set; }
    }
}
