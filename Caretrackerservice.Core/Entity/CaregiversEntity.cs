using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("Caregivers")]
    public class CaregiversEntity : Document
    {
        public int CAREGIVER_ID { get; set; }
        public int HHA { get; set; }
        public int CaregiverAdditionalDetailID { get; set; }
        public string HIRED_DATE { get; set; }
        public string TERMINATED_DATE { get; set; }
        public int PAYROLL_BRANCH_ID { get; set; }
        public string STATUS { get; set; }
        public string FIRST_NAME { get; set; }
        public string MIDDLE_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string SSN { get; set; }
        public string DISCIPLINE { get; set; }
        public string COUNTY { get; set; }
        public string TelephonyID { get; set; }
        public int CAREGIVER_DISC_ID { get; set; }
        public string EvvCaregiverDiscipline { get; set; }
        public string CaregiverDiscipline { get; set; }
        public string BASE_DISCIPLINE { get; set; }
        public int BASE_DISCIPLINE_ID { get; set; }
        public string ADDRESS_ID { get; set; }
        public string ADDRESS_1 { get; set; }
        public string ADDRESS_2 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIPCODE { get; set; }
        public string TELEPHONE { get; set; }
        public string WORK_PHONE { get; set; }
        public string FAX { get; set; }
        public string EMAIL { get; set; }
        public string ALTERNATE_PHONE { get; set; }
        public List<CaregiverLocationsEntity> CaregiverLocationsEntity { get; set; }
        public List<CM2K_CliniciansEntity> CM2K_CliniciansEntity { get; set; }
    }
}