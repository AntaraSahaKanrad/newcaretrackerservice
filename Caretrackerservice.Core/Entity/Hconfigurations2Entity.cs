﻿using System;
using System.Collections.Generic;
using System.Text;
using Caretrackerservice.Core.Entity.BaseEntity;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("HConfigurations2")]
    public class Hconfigurations2Entity:Document
    {
        public int ClientAppID { get; set; }

        public int HHA_ID { get; set; }
        public string CMS_CERT_NO { get; set; }
        public string NATL_PROVIDER_ID { get; set; }
        public string TAXONOMY_CODE { get; set; }
        public string FED_TAX_NO { get; set; }

        public int HHA_BRANCHID { get; set; }
        public string TIME_ZONE { get; set; } 

    }
}
