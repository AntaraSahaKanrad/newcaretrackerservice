﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("EvvConfiguration")]
    public class EvvConfigurationEntity : Document
    {
        public int EvvConfigurationID { get; set; }
        public int HHASelectedEvvMasterID { get; set; }
        public int HHABranchID { get; set; }
        public int EvvVendorVersionMasterID { get; set; }
        public string ConfigurationJSON { get; set; }
        public int? HHA { get; set; }
        public DateTime? EffectiveDate { get; set; }
    }
}
