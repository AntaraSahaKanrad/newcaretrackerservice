﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("ClientadditionalDeatils")]
    public class ClientAdditionalDetailsEntity : Document
    {
        public int ClientAppID { get; set; }
        public int CLIENT { get; set; }
        public int HHA { get; set; }
        public DateTime ReferralDate { get; set; }
        public string TelephonyID { get; set; }
        public string EVVClientID { get; set; }
        public string County { get; set; }
        public string EVVPatientRegion { get; set; }
        public string EVVPatientSDA { get; set; } 

    }
}
