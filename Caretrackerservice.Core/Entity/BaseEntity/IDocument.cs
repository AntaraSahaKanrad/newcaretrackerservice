﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity.BaseEntity
{
    public interface IDocument
    {
        [BsonId]
        [BsonRepresentation(BsonType.String)]
        ObjectId _id { get; set; }

       // DateTime CreatedOn { get; }
    }
}
