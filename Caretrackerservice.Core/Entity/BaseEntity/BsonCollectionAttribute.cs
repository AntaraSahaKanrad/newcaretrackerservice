﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity.BaseEntity
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class BsonCollectionAttribute : Attribute
    {
        public string CollectionName { get; }

        public BsonCollectionAttribute(string collectionName)
        {
            CollectionName = collectionName;
        }
    }
}
