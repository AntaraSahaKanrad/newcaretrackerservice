﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity.BaseEntity
{
    public abstract class Document : IDocument
    {
        public ObjectId _id { get; set; }
        public int ModifiedDataMasterID { get; set; }
        public DateTime ModifiedDataCreatedOn { get; set; }
        public DateTime MongoCreatedDatetime { get; set; } = DateTime.UtcNow;
    }
}
