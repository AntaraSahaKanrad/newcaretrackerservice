﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("PayerServices")]
    public class PayerServicesEntity: Document
    {
        public int SERVICE_CODE_ID { get; set; }
        public string DESCRIPTION { get; set; }
        public int HHA { get; set; }
        public bool ACTIVE { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public DateTime LastModified_On { get; set; }
        public string EVVServiceGroupCode { get; set; }
    }
}
