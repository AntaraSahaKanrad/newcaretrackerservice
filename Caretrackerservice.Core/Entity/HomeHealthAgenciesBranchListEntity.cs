﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("HomeHealthAgenciesBranchList")]
    public class HomeHealthAgenciesBranchListEntity:Document
    {
        public int HHA { get; set; }
        public string BRANCH_ID { get; set; }
        public string CMS_CERT_NO { get; set; }
        public string NATL_PROVIDER_ID { get; set; }
        public string TAXONOMY_CODE { get; set; }
        public string FED_TAX_NO { get; set; }
        public int HHA_BRANCH_ID { get; set; }
        public string ADDRESS_ID { get; set; }
        public string ADDRESS_1 { get; set; }
        public string ADDRESS_2 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIPCODE { get; set; }
        public string TELEPHONE { get; set; }
        public string WORK_PHONE { get; set; }
        public string FAX { get; set; }
        public string EMAIL { get; set; }
        public string ALTERNATE_PHONE { get; set; }
    }
}
