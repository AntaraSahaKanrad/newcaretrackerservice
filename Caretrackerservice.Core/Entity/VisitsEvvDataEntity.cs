﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
   public class VisitsEvvDataEntity
    {
        public DateTime? OriginalCheckinTime { get; set; }

        public DateTime? OriginalCheckoutTime { get; set; }
        public string CheckinGeoCoordinates { get; set; }
        public string CheckoutGeoCoordinates { get; set; }
        public string CheckinTelephonyID { get; set; }
        public string CheckoutTelephonyID { get; set; }
        public int CheckinSource { get; set; }
        public int CheckoutSource { get; set; }
        public bool isClientSignatureAvailable { get; set; }
        public bool isEvvEnabled { get; set; }
        public int EvvVendorUID { get; set; }
        public string EvvVendorDisplayName { get; set; }

    }
}
