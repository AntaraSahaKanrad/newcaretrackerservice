﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("EvvVendorVersionMaster")]
    public class EvvVendorVersionMasterEntity : Document
    {
        public int EvvVendorVersionMasterID { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int EvvVendorMasterID { get; set; }
        public string DisplayName { get; set; }
        public string VendorState { get; set; }
        public string VendorVersionCode { get; set; }
        public string VendorConfigTemplate { get; set; }
        public bool? AutoGenerateException { get; set; }
        public bool? AttestationMandatoryForAllReasonCodes { get; set; }
        public bool? IsLocationUploadRequired { get; set; }
        public bool? IsPayerUploadRequired { get; set; }
        public bool? IsClientUploadRequired { get; set; }
        public bool? IsAuthorizationUploadRequired { get; set; }
        public bool? IsCaregiverUploadRequired { get; set; }
        public bool? IsScheduleUploadRequired { get; set; }
        public bool? IsExport { get; set; }
        public string ConfigurationsJson { get; set; }
        public string AppUniqueCode { get; set; }
        public string ScheduleStatus { get; set; }
        public int? MaxSchedulesToPush { get; set; }
        public int? MaxClientsToPush { get; set; }
        public int? MaxCaregiversToPush { get; set; }
        public int? MaxPayersToPush { get; set; }
        public int? MaxAuthorizationToPush { get; set; }
        public int? MaxSchedulesToDownload { get; set; }
    }
}
