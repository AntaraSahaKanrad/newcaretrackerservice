using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    public class CM2K_CliniciansEntity
    {
        public int HHA { get; set; }
        public int cm2kClinicianID { get; set; }
        public string CM2kUniqueClinicianID { get; set; }
        public bool isdirty { get; set; }
        public string EVVVendorUniqueClinicianID { get; set; }
        public int CaregiverID { get; set; }
    }
}
