﻿using System;
using System.Collections.Generic;
using System.Text;
using Caretrackerservice.Core.Entity.BaseEntity;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("ClientPaymentSources")]
    public class ClientPaymentSourcesEntity : Document
    {
        public int ClientAppID { get; set; }
        public int CLIENT_PAYMENT_ID { get; set; }
        public int CLIENT_ID { get; set; }

        public string PAYMENT_SOURCE_ID { get; set; }
        public int HHA { get; set; }
        public string CreatedOn { get; set; } 
    }
}