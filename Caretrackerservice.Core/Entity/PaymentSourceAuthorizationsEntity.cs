﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("PaymentSourceAuthorizations")]
    public class PaymentSourceAuthorizationsEntity : Document
    {
        public int AUTHORIZATION_ID { get; set; }
        public int PAYMENT_SOURCE { get; set; }
        public int CLIENT { get; set; }
        public string AUTHORIZATION_NO { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool isOfficeAuth { get; set; }
        public int HHA { get; set; }
    }
}
