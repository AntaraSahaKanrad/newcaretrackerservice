﻿using Caretrackerservice.Core.Entity.BaseEntity;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("Visits")]
    public class VisitsEntity : Document
    {
         
        public VisitsBasicsEntity  visitsBasicsEntity {get; set;}
        public VisitsClientEntity visitsClientEntity { get; set; }
        public VisitsInfoEntity visitsInfoEntity { get; set; }
        public VisitsEvvDataEntity visitsEvvDataEntity { get; set; }
        public VisitsCaregiverEntity visitsCaregiverEntity { get; set; }
        public IEnumerable<VisitsChildEntity> visitsChildEntity { get; set; }
        public VisitsBillingEntity visitsBillingEntity { get; set; }
        public VisitsPayrollEntity visitsPayrollEntity { get; set; }
        public VisitsPayerEntity visitsPayerEntity { get; set; }
        public VisitsServiceEntity visitsServiceEntity { get; set; }
        public VisitsEvvTransmissionEntity visitsEvvTransmissionEntity { get; set; }
        public IEnumerable<VisitsReasonsEntity> visitsReasonsEntity { get; set; }
        public IEnumerable<VisitsExceptionsEntity> visitsExceptionsEntity { get; set; }
    }
}
