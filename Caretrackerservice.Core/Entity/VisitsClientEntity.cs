﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    public class VisitsClientEntity
    {
        public long ClientUID { get; set; }
        public int ClientAppID { get; set; }
        public string ClientBranchName { get; set; }
        public string ClientBranchID { get; set; }
        public string ClientDisplayName { get; set; }
        public string ClientStatus { get; set; }

    }
}
