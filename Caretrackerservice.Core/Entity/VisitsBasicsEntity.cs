﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
public  class VisitsBasicsEntity
    {
        public DateTime createdOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public string CreatedByUserName { get; set; }
        public string ApplicationCode { get; set; }
        public int HHA { get; set; }
        public Int64 ScheduleAppID { get; set; }
    }
}
