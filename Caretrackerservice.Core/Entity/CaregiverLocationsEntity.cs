using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    public class CaregiverLocationsEntity
    {
        public int CaregiverLocationID { get; set; }
        public int HHA { get; set; }
        public int HHA_BranchID { get; set; }
        public bool isDeleted { get; set; }
        public int Caregiver { get; set; }
        public DateTime? DeletedOn { get; set; }
        public int? DeletedBy { get; set; }
        public string BRANCH_ID { get; set; }
    }
}