﻿using System;
using System.Collections.Generic;
using System.Text;
using Caretrackerservice.Core.Entity.BaseEntity;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("ClientEmergencyContact")]
    public class ClientEmergencyContactEntity: Document
    {
        public int ClientAppID { get; set; }
        public int EMERGENCY_CONTACT_ID { get; set; }
        public int CLIENT { get; set; }

        public string CONTACT_FIRST_NAME { get; set; }
        public string CONTACT_LAST_NAME { get; set; }
        public int HHA { get; set; }
        public string CreatedOn { get; set; }
        public bool IsPatientRepresentative { get; set; }

        //public int ADDRESS { get; set; }
        //Address Fileds 
        public string ADDRESS_1 { get; set; }
        public string ADDRESS_2 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string TELEPHONE { get; set; }
        public string ZIPCODE { get; set; }
        public string MOBILE { get; set; }
        public string ALTERNATE_PHONE { get; set; }
        public string WORK_PHONE { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string EMAIL { get; set; } 

    }
}
