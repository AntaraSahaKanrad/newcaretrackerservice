﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{   
    [BsonCollection("ClientTreatmentAddress")]
    public class ClientTreatmentAddressEntity : Document
    {
        public int ClientAppID { get; set; }
        public int ClientID { get; set; }
        public int TreatmentAddressID { get; set; }
        public string LocationName { get; set; }
        public bool isPrimaryTreatmentAddress { get; set; } 

    }
}
