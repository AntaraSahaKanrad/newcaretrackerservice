﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("CaregiverTaskChildSchedules")]
    public class CaregiverTaskChildSchedulesEntity : Document
    {
        public int Child_Schedule_Id { get; set; }
        public int HHA { get; set; }
        public int PARENT_CGTASK_ID { get; set; }
        public int SERVICECODE_ID { get; set; }
        public int PAYMENT_SOURCE { get; set; }
        public bool isEvvschedule { get; set; }
        public bool isEvvScheduleDirty { get; set; }
        public DateTime? ACTUAL_STARTTIME { get; set; }
        public DateTime? ACTUAL_ENDTIME { get; set; }
    }
}
