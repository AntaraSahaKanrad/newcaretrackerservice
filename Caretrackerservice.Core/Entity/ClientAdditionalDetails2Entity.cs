﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("ClientadditionalDeatils2")]
    public class ClientAdditionalDetails2Entity : Document
    {
        public int ClientAppID { get; set; }
        public int CLIENT { get; set; }
        public int HHA { get; set; }
        public string EVVCCUContractNumber { get; set; } 

    }
}
