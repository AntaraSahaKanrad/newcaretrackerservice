﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("PaymentSourceBranches")]
    public class PaymentSourceBranchesEntity: Document
    {
        public int PaymentSource_BranchID { get; set; }
        public int PaymentSource_ID { get; set; }
        public int HHA { get; set; }
        public int Branch_ID { get; set; }
        public DateTime CreatedOn { get; set; }
        public int EvvAggregatorVendorVersionMasterID { get; set; }
    }
}
