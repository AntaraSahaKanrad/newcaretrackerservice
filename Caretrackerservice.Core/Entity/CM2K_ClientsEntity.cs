﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("CM2K_Clients")]
    public class CM2K_ClientsEntity : Document
    {
        public int ClientAppID { get; set; }
        public string CM2kUniqueClientID { get; set; }
        public int EVVVendorUniqueClientID { get; set; }
        public int EvvVendorVersionMasterID { get; set; } 

    }
}
