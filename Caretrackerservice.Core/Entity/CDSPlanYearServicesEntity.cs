﻿using Caretrackerservice.Core.Entity.BaseEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Core.Entity
{
    [BsonCollection("CDSPlanYearServicesAuthorization")]
    public class CDSPlanYearServicesEntity:Document
    {
        public int CDSPlanYearServiceID { get; set; }
        public int HHA { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ClientID { get; set; }
        public int PayerCDSServiceCategoryID { get; set; }
        public string HCPCS { get; set; }
        public string Modifer1 { get; set; }
        public string Modifer2 { get; set; }
        public string Modifer3 { get; set; }
        public string Modifer4 { get; set; }
        public string TaxHCPCS { get; set; }
        public string TaxModifiers1 { get; set; }
        public string TaxModifiers2 { get; set; }
        public string TaxModifiers3 { get; set; }
        public string TaxModifiers4 { get; set; }
        public int PayerID { get; set; }
        public string Status { get; set; }
        public bool IsEvvDirty { get; set; }
    }
}
