FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY . .
RUN dotnet restore "Caretrackerservice.API/Caretrackerservice.API.csproj"  --configfile ./NuGet/NuGet.Config  -s https://api.nuget.org/v3/index.json -s https://phcdemo1.kantimemedicare.net/KantimeNuget/nuget -verbosity:normal
WORKDIR "/src/Caretrackerservice.API"
RUN dotnet build "Caretrackerservice.API.csproj" -c Release -o /app
RUN dotnet publish "Caretrackerservice.API.csproj" -c Release -o /app
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
RUN sed -i '/^ssl_conf = ssl_sect$/s/^/#/' /etc/ssl/openssl.cnf
WORKDIR /app
COPY --from=build /app ./
ENTRYPOINT ["dotnet", "Caretrackerservice.API.dll"]
