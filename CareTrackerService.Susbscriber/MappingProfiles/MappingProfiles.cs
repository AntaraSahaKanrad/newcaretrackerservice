﻿using AutoMapper;
using Caretrackerservice.Core.Entity;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.MappingProfiles
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Clients, ClientsEntity>().ReverseMap();
            CreateMap<ClientAdditionalDetails, ClientAdditionalDetailsEntity>().ReverseMap();
            CreateMap<ClientAdditionalDetails2, ClientAdditionalDetails2Entity>().ReverseMap();
            
            CreateMap<Caregivers, CaregiversEntity>().ReverseMap();
            CreateMap<HomeHealthAgencies, HomeHealthAgenciesEntity>().ReverseMap();
            CreateMap<HomeHealthAgenciesBranchList, HomeHealthAgenciesBranchListEntity>().ReverseMap();
            CreateMap<PaymentSources, PaymentSourcesEntity>().ReverseMap();
            CreateMap<PaymentSourceBranches, PaymentSourceBranchesEntity>().ReverseMap();
            CreateMap<PayerServices, PayerServicesEntity>().ReverseMap();

            CreateMap<_C_EvvConfigurations, EvvConfigurationEntity>().ReverseMap();
            CreateMap<_UA_EVVVendorMaster, EvvVendorMasterEntity>().ReverseMap();
            CreateMap<_UA_EvvVendorVersionMaster, EvvVendorVersionMasterEntity>().ReverseMap();

            CreateMap<CaregiverLocations, CaregiverLocationsEntity>().ReverseMap();
            CreateMap<CM2K_Clinicians, CM2K_CliniciansEntity>().ReverseMap();
            CreateMap<ClientEmergencyContact, ClientEmergencyContactEntity>().ReverseMap();
            CreateMap<Hconfigurations2, Hconfigurations2Entity>().ReverseMap();

            CreateMap<CM2K_Clients, CM2K_ClientsEntity>().ReverseMap();
            CreateMap<ClientTreatmentAddress, ClientTreatmentAddressEntity>().ReverseMap();
            CreateMap<ClientPaymentSources, ClientPaymentSourcesEntity>().ReverseMap();

            CreateMap<CaregiverTaskChildSchedules, CaregiverTaskChildSchedulesEntity>().ReverseMap();
            CreateMap<CaregiverTaskEvvExceptions, CaregiverTaskEvvExceptionsEntity>().ReverseMap();
            CreateMap<CaregiverTaskEvvReasons, CaregiverTaskEvvReasonsEntity>().ReverseMap();

            CreateMap<PaymentSourceAuthorizations, PaymentSourceAuthorizationsEntity>().ReverseMap();
            CreateMap<CDSPlanYearServices, CDSPlanYearServicesEntity>().ReverseMap();

            CreateMap<CaregiverTasks, CaregiverTaskEntity>().ReverseMap();
        }
    }
}
