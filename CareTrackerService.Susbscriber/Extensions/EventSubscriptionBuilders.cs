﻿using CareTrackerService.Susbscriber.EventHandlers;
using EventBus.MQ;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Extensions
{
    public static class EventSubscriptionBuilders
    {
        public static void ConfigEventSubscription(this IApplicationBuilder app, IConfiguration configuration)
        {

            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();          


            if (configuration["MessagingQ"] == "1")
            {
                var RoutingKey = configuration["KafkaTopicName"];

                eventBus.SubscribeDynamic<DynamicEventHandlers>(RoutingKey);
            }
            else
            {


                var QueueNameRabbitMQ = configuration["QueueNameRabbitMQ"];
                var RoutingKeyRabbitMQ = configuration["RoutingKeyRabbitMQ"];

                eventBus.SubscribeDynamic<DynamicEventHandlers>(RoutingKeyRabbitMQ, QueueNameRabbitMQ);
            }


            eventBus.StartConsumer();
        }
    }
}
