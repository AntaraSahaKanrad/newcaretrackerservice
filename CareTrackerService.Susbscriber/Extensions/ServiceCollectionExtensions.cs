﻿using Autofac;
using AutoMapper;
using CareTrackerService.Susbscriber.EnumValues;
using CareTrackerService.Susbscriber.EventHandlers;
using CareTrackerService.Susbscriber.Services;
using CareTrackerService.Susbscriber.Services.Contracts;
using EventBus.MQ;
using EventBus.MQ.Kafka;
using EventBus.MQ.RabbitMQ;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CareTrackerService.Susbscriber
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSusbscriberServiceCollection(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            if (configuration["MessagingQ"] == "1")
            {                

                services.AddSingleton<IEventBus, EventBusKafkaMQ>(cp =>
                {
                    var kafkaConnection = cp.GetRequiredService<IMQConnection>();
                    var iLifetimeScope = cp.GetRequiredService<ILifetimeScope>();
                    var logger = cp.GetRequiredService<ILogger<EventBusKafkaMQ>>();
                    return new EventBusKafkaMQ(kafkaConnection, logger, iLifetimeScope);
                });


                services.AddSingleton<IMQConnection>(cp =>
                {
                    var logger = cp.GetRequiredService<ILogger<KafkaMQConnection>>();
                    var kafkaconectionstring = configuration["Kafkaconstring"];
                    return new KafkaMQConnection(kafkaconectionstring, logger, 5);
                });



            }
            else
            {
                services.AddSingleton<IMQConnection>(sp =>
                {
                    var logger = sp.GetRequiredService<ILogger<RabbitMQConnection>>();
                    var RabitMQconectionstring = configuration["rabitmqconstring"];
                    return new RabbitMQConnection(RabitMQconectionstring, logger, 5);
                });


                services.AddSingleton<IEventBus, EventBusRabbitMQ>(sp =>
                {
                    var rabbitMQConnection = sp.GetRequiredService<IMQConnection>();
                    var iLifetimeScope = sp.GetRequiredService<ILifetimeScope>();
                    var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
                    return new EventBusRabbitMQ(rabbitMQConnection, logger, iLifetimeScope);
                });
            }             

             
            services.AddTransient<DynamicEventHandlers>();

             
            services.AddScoped<ScheduleSubscribePayload>();
            services.AddScoped<ClientSubscribePayload>();
            services.AddScoped<ClientAdditionalDetailsSubscribePayload>();
            services.AddScoped<ClientAdditionalDetails2SubscribePayload>();
            services.AddScoped<CaregiversSubscribePayload>();
            services.AddScoped<HomeHealthAgenciesSubscribePayload>();
            services.AddScoped<HomeHealthAgenciesBranchListSubscribePayload>();
            services.AddScoped<PaymentSourcesSubscribePayload>();
            services.AddScoped<PaymentSourceBranchesSubscribePayload>();
            services.AddScoped<PayerServicesSubscribePayload>();
            services.AddScoped<EvvConfigurationSubscriberPayload>();
            services.AddScoped<EvvVendorMasterSubscribePayload>();
            services.AddScoped<EvvVendorVersionasterSubscribePayload>();
            services.AddScoped<ClientEmergencyContactSubscribePayload>();
            services.AddScoped<Hconfigurations2SubscribePayload>();
            services.AddScoped<CM2K_ClientsSubscribePayload>();
            services.AddScoped<ClientTreatmentAddressSubscribePayload>();
            services.AddScoped<ClientPaymentSourcesSubscribePayload>();
            services.AddScoped<ScheduleSplitSubscribePayload>();
            services.AddScoped<AuthorizationSubscribePayload>();
            services.AddScoped<CDSAuthSubscribePayload>();
            services.AddScoped<ScheduleEvvExceptionsSubscribePayload>();
            services.AddScoped<ScheduleEvvReasonsSubscribePayload>();
            services.AddScoped<ISubscribePayloadMaster, SubscribePayloadMaster>(); 

            return services;
        }
    }
}
