﻿using CareTrackerService.Susbscriber.EnumValues;
using CareTrackerService.Susbscriber.Services.Contracts;
using EventBus.MQ;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CareTrackerService.Susbscriber.EventHandlers
{
    public class DynamicEventHandlers : IDynamicIntegrationEventHandler
    {
        protected readonly ISubscribePayloadMaster  _subscribePayloadMaster;
        private readonly ILogger<DynamicEventHandlers> _logger;
        public DynamicEventHandlers(ISubscribePayloadMaster subscribePayloadMaster, ILogger<DynamicEventHandlers> logger)
        {
            _subscribePayloadMaster = subscribePayloadMaster;
            _logger = logger;
        }

        public async Task Handle(dynamic @event)
        {
            string ModifiedType = @event.ModifiedDataContext;
           
            if (Enum.IsDefined(typeof(ModifiedDataTypeContext), ModifiedType))
            {
                _logger.LogInformation($"{ModifiedType} context handling subscriber");
                ModifiedDataTypeContext modifiedDataType = (ModifiedDataTypeContext)Enum.Parse(typeof(ModifiedDataTypeContext), ModifiedType);

                await this._subscribePayloadMaster.Create(modifiedDataType).GetSubscribedPayload(@event);

            }
            else
            {
                _logger.LogInformation($"{ModifiedType} context Not able to handle in cartracker service");
            }
        }
    }
}
