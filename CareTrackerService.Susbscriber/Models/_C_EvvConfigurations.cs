﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class _C_EvvConfigurations:BaseModel
    {
        public int EvvConfigurationID { get; set; }
        public int HHASelectedEvvMasterID { get; set; }
        public int HHABranchID { get; set; }
        public int EvvVendorVersionMasterID { get; set; }
        public string ConfigurationJSON { get; set; }
        public int? HHA { get; set; }
        public DateTime? EffectiveDate { get; set; }
       
    }
}
