﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class HomeHealthAgencies : BaseModel
    {
        public int HHA_ID { get; set; }
        public string HHA_NAME { get; set; }
        public string ADDRESS_ID { get; set; }
        public string ADDRESS_1 { get; set; }
        public string ADDRESS_2 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string ZIPCODE { get; set; }
        public string TELEPHONE { get; set; }
        public string WORK_PHONE { get; set; }
        public string FAX { get; set; }
        public string EMAIL { get; set; }
        public string ALTERNATE_PHONE { get; set; }
    }
}
