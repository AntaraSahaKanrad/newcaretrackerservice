﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class ClientPaymentSources : BaseModel
    {
        public int ClientAppID { get; set; }
        public int CLIENT_PAYMENT_ID { get; set; }
        public int CLIENT_ID { get; set; }

        public string PAYMENT_SOURCE_ID { get; set; }
        public int HHA { get; set; }
        public string CreatedOn { get; set; } 
    }
}
