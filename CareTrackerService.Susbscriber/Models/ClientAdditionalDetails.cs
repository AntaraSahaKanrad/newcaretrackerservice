﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class ClientAdditionalDetails : BaseModel
    {
        public int ClientAppID { get; set; }
        public int CLIENT { get; set; }
        public int HHA { get; set; }
        public DateTime ReferralDate { get; set; }
        public string TelephonyID { get; set; }
        public string EVVClientID { get; set; }
        public string County { get; set; }
        public string EVVPatientRegion { get; set; }
        public string EVVPatientSDA { get; set; } 

    }
}
