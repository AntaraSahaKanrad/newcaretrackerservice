﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class CM2K_Clients : BaseModel
    {
        public int ClientAppID { get; set; }
        public string CM2kUniqueClientID { get; set; }
        public int EVVVendorUniqueClientID { get; set; }
        public int EvvVendorVersionMasterID { get; set; } 

    }
}
