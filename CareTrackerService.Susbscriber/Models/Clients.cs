﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class Clients : BaseModel
    {
        public int ClientAppID { get; set; }
        public int CLIENT_ID { get; set; }
        public int HHA { get; set; }
        public string PATIENT_ID { get; set; }
        public string FIRST_NAME { get; set; }
        public string MIDDLE_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string GENDER { get; set; }
        public string SSN { get; set; }
        public string MEDICAID_NO { get; set; }
        public DateTime DATE_OF_BIRTH { get; set; }
        public string STATUS { get; set; }
        public int HHA_BRANCH_ID { get; set; }
        public int ClientGroupNo { get; set; }
        public DateTime SOCDate { get; set; }
        public DateTime DischargeDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }

        //Address Fileds 
        public string ADDRESS_1 { get; set; }
        public string ADDRESS_2 { get; set; }
        public string CITY { get; set; }
        public string STATE { get; set; }
        public string TELEPHONE { get; set; }
        public string ZIPCODE { get; set; }
        public string MOBILE { get; set; }
        public string ALTERNATE_PHONE { get; set; }
        public string WORK_PHONE { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string EMAIL { get; set; } 

    }
}
