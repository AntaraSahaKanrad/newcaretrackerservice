﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class ClientAdditionalDetails2 : BaseModel
    {
        public int ClientAppID { get; set; }
        public int CLIENT { get; set; }
        public int HHA { get; set; }
        public string EVVCCUContractNumber { get; set; } 

    }
}
