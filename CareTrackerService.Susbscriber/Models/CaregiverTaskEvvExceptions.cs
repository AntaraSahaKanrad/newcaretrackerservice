﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class CaregiverTaskEvvExceptions : BaseModel
    {
        public int CaregiverTaskEvvExceptionID { get; set; }
        public int HHA { get; set; }
        public int CgTaskID { get; set; }
        public string Context { get; set; }
        public string ExceptionCode { get; set; }
        public string ExceptionReason { get; set; }
        public string Notes { get; set; }
        public bool isResolved { get; set; }
        public DateTime? ResolvedOn { get; set; }
        public int ResolvedBy { get; set; }
        public int CaregiverTaskEvvReasonID { get; set; }
        public string SystemCode { get; set; }
        public bool isSystemAutoAdded { get; set; }
        public int CaregiverTaskChildID { get; set; }
    }
}
