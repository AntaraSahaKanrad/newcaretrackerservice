﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class CaregiverTaskEvvReasons : BaseModel
    {
        public int CaregiverTaskEvvReasonID { get; set; }
        public int CgTaskID { get; set; }
        public int HHA { get; set; }
        public bool requireAttestation { get; set; }
        public string CheckinCheckoutType { get; set; }
        public bool isAttested { get; set; }
        public int AttestedBy { get; set; }
        public DateTime? AttestedOn { get; set; }
        public string Reason { get; set; }
        public string ReasonContext { get; set; }
        public string Notes { get; set; }
        public string ReasonCode { get; set; }
        public int CaregiverTaskChildID { get; set; }
        public bool isDeleted { get; set; }
    }
}
