﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class Hconfigurations2 : BaseModel
    {
        public int ClientAppID { get; set; }

        public int HHA_ID { get; set; }
        public string CMS_CERT_NO { get; set; }
        public string NATL_PROVIDER_ID { get; set; }
        public string TAXONOMY_CODE { get; set; }
        public string FED_TAX_NO { get; set; }

        public int HHA_BRANCHID { get; set; }
        public string TIME_ZONE { get; set; } 

    }
}
