﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class PaymentSourceBranches : BaseModel
    {
        public int PaymentSource_BranchID { get; set; }
        public int PaymentSource_ID { get; set; }
        public int HHA { get; set; }
        public int Branch_ID { get; set; }
        public DateTime CreatedOn { get; set; }
        public int EvvAggregatorVendorVersionMasterID { get; set; }
    }
}
