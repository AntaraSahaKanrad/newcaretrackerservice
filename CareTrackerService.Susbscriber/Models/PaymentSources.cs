﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class PaymentSources : BaseModel
    {
        public int PAYMENT_SOURCE_ID { get; set; }
        public string ORG_NAME { get; set; }
        public int HHA { get; set; }
        public bool ACTIVE { get; set; }
        public int Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public DateTime LastModified_On { get; set; }
        public bool IsEnableEVV { get; set; }
        public bool IsEVVDirty { get; set; }
        public int EvvAggregatorVendorVersionMasterID { get; set; }
    }
}
