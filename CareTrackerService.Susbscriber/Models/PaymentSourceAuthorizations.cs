﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class PaymentSourceAuthorizations : BaseModel
    {
        public int AUTHORIZATION_ID { get; set; }
        public int PAYMENT_SOURCE { get; set; }
        public int CLIENT { get; set; }
        public string AUTHORIZATION_NO { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool isOfficeAuth { get; set; }
        public int HHA { get; set; }
    }
}
