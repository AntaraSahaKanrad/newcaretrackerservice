﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class BaseModel
    { 
        public int ModifiedDataMasterID { get; set; }
        public DateTime ModifiedDataCreatedOn { get; set; }
    }
}
