﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class CaregiverTaskChildSchedules : BaseModel
    {
        public int Child_Schedule_Id { get; set; }
        public int HHA { get; set; }
        public int PARENT_CGTASK_ID { get; set; }
        public int SERVICECODE_ID { get; set; }
        public int PAYMENT_SOURCE { get; set; }
        public bool isEvvschedule { get; set; }
        public bool isEvvScheduleDirty { get; set; }
        public DateTime? ACTUAL_STARTTIME { get; set; }
        public DateTime? ACTUAL_ENDTIME { get; set; }
    }
}
