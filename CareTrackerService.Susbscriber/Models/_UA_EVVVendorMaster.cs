﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public  class _UA_EVVVendorMaster: BaseModel
    {
        public int EvvVendorMasterID { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string VendorName { get; set; }
        public string VendorCode { get; set; }
        public bool isPrimary { get; set; }
    }
}
