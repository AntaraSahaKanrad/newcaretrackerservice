﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class ClientTreatmentAddress : BaseModel
    {
        public int ClientAppID { get; set; }
        public int ClientID { get; set; }
        public int TreatmentAddressID { get; set; }
        public string LocationName { get; set; }
        public bool isPrimaryTreatmentAddress { get; set; } 

    }
}
