﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Models
{
    public class CaregiverDisciplines : BaseModel
    {
        public int CAREGIVER_DISC_ID { get; set; }
        public string EvvCaregiverDiscipline { get; set; }
        public string DISCIPLINE { get; set; }
        public string BASE_DISCIPLINE { get; set; }
        public int BASE_DISCIPLINE_ID { get; set; }
    }
}
