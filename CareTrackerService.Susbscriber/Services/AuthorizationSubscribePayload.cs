﻿using AutoMapper;
using Caretrackerservice.Core.Entity;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using CareTrackerService.Susbscriber.Models;
using CareTrackerService.Susbscriber.Services.Contracts;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CareTrackerService.Susbscriber.Services
{
    public class AuthorizationSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<PaymentSourceAuthorizationsEntity> _commonRepository;
        private readonly ILogger<AuthorizationSubscribePayload> _logger;
        public AuthorizationSubscribePayload(IMapper mapper, ICommonRepository<PaymentSourceAuthorizationsEntity> commonRepository, ILogger<AuthorizationSubscribePayload> logger)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
            _logger = logger;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {
            if (@event == null || @event.Payload == null)
                return false;

            try
            {
                return await Task.Run(() =>
                {

                    var  AuthorizationsPayload = JsonConvert.SerializeObject(@event.Payload);
                    var Authorizations  = JsonConvert.DeserializeObject<IEnumerable<PaymentSourceAuthorizations>>(AuthorizationsPayload);

                    List<PaymentSourceAuthorizations> paymentSourcesList = Authorizations;

                    var AuthorizationsEntity = _mapper.Map<IEnumerable<PaymentSourceAuthorizationsEntity>>(paymentSourcesList);

                    _commonRepository.CreateCollectionData(AuthorizationsEntity);


                    return true;
                });

            }
            catch (Exception ex)
            {
                _logger.LogError("AuthorizationSubscribePayload- GetSubscribedPayload : " + ex.ToString());
                return false;
            }
        }
    }
}
