﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
    public class EvvConfigurationSubscriberPayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<EvvConfigurationEntity> _commonRepository;
        public EvvConfigurationSubscriberPayload(IMapper mapper, ICommonRepository<EvvConfigurationEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }

        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                { 
                        var EvvconfigPayload = JsonConvert.SerializeObject(@event.Payload);
                        var Evvconfigurations = JsonConvert.DeserializeObject<IEnumerable<EvvConfigurationEntity>>(EvvconfigPayload);

                        List<EvvConfigurationEntity> evvconfiglist = Evvconfigurations;

                        var evvconfigEntity = _mapper.Map<IEnumerable<EvvConfigurationEntity>>(evvconfiglist);

                        _commonRepository.CreateCollectionData(evvconfiglist);
                     
                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
