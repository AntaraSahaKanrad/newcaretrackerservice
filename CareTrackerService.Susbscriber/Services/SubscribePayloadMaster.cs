﻿using CareTrackerService.Susbscriber.EnumValues;
using CareTrackerService.Susbscriber.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Services
{
    public class SubscribePayloadMaster: ISubscribePayloadMaster
    {
        private readonly ClientSubscribePayload _clientSubscribePayload;
        private readonly ScheduleSubscribePayload _scheduleSubscribePayload;
        private readonly CaregiversSubscribePayload _caregiversSubscribePayload;
        private readonly HomeHealthAgenciesSubscribePayload _homeHealthAgenciesSubscribePayload;
        private readonly HomeHealthAgenciesBranchListSubscribePayload _homeHealthAgenciesBranchListSubscribePayload;
        private readonly PaymentSourcesSubscribePayload _paymentSourcesSubscribePayload;
        private readonly PaymentSourceBranchesSubscribePayload _paymentSourceBranchesSubscribePayload;
        private readonly PayerServicesSubscribePayload _payerServicesSubscribePayload;
        private readonly EvvConfigurationSubscriberPayload _evvconfigSubscribePayload;
        private readonly EvvVendorMasterSubscribePayload _evvvendormasterSubscribePayload;
        private readonly EvvVendorVersionasterSubscribePayload _evvvendorversionmasterSubscribePayload;
        private readonly ClientAdditionalDetailsSubscribePayload _clientadditionalsubscibePayload;
        private readonly ClientAdditionalDetails2SubscribePayload _clientadditional2subscibePayload;        
        private readonly Hconfigurations2SubscribePayload _Hconfigurations2SubscribePayload;
        private readonly ClientEmergencyContactSubscribePayload _ClientEmergencyContactSubscribePayload;
        private readonly CM2K_ClientsSubscribePayload _cM2K_ClientsSubscribePayload;
        private readonly ClientTreatmentAddressSubscribePayload _clientTreatmentAddressSubscribePayload;
        private readonly ClientPaymentSourcesSubscribePayload _ClientPaymentSourcesSubscribePayload;
        private readonly ScheduleSplitSubscribePayload _scheduleSplitSubscribePayload;
        private readonly AuthorizationSubscribePayload _authorizationSubscribePayload;
        private readonly CDSAuthSubscribePayload _cDSAuthSubscribePayload;
        private readonly ScheduleEvvReasonsSubscribePayload _scheduleEvvReasonsSubscribePayload;
        private readonly ScheduleEvvExceptionsSubscribePayload _scheduleEvvExceptionsSubscribePayload;
        private readonly Dictionary<ModifiedDataTypeContext, ISubscribe> _modifiedDataTypeContextfactories;

        public SubscribePayloadMaster(ScheduleSubscribePayload scheduleSubscribePayload, ClientSubscribePayload clientSubscribePayload, 
            CaregiversSubscribePayload caregiversSubscribePayload, HomeHealthAgenciesSubscribePayload homeHealthAgenciesSubscribePayload,
            HomeHealthAgenciesBranchListSubscribePayload homeHealthAgenciesBranchListSubscribePayload,
            PaymentSourcesSubscribePayload paymentSourcesSubscribePayload, PaymentSourceBranchesSubscribePayload paymentSourceBranchesSubscribePayload,
            PayerServicesSubscribePayload payerServicesSubscribePayload, EvvConfigurationSubscriberPayload evvconfigSubscribePayload, EvvVendorMasterSubscribePayload evvvendormasterSubscribePayload,
            EvvVendorVersionasterSubscribePayload evvvendorversionmasterSubscribePayload,ClientAdditionalDetailsSubscribePayload clientadditionalsubscibePayload,
            ClientAdditionalDetails2SubscribePayload clientAdditionalDetails2SubscribePayload, Hconfigurations2SubscribePayload Hconfigurations2SubscribePayload, 
            ClientEmergencyContactSubscribePayload ClientEmergencyContactSubscribePayload,      
            CM2K_ClientsSubscribePayload cM2K_ClientsSubscribePayload ,ClientTreatmentAddressSubscribePayload clientTreatmentAddressSubscribePayload,
            ClientPaymentSourcesSubscribePayload ClientPaymentSourcesSubscribePayload, ScheduleSplitSubscribePayload scheduleSplitSubscribePayload, 
            AuthorizationSubscribePayload authorizationSubscribePayload, CDSAuthSubscribePayload cDSAuthSubscribePayload, ScheduleEvvReasonsSubscribePayload scheduleEvvReasonsSubscribePayload,
            ScheduleEvvExceptionsSubscribePayload scheduleEvvExceptionsSubscribePayload)
        {
            _modifiedDataTypeContextfactories = new Dictionary<ModifiedDataTypeContext, ISubscribe>();
            _clientSubscribePayload = clientSubscribePayload;
            _scheduleSubscribePayload = scheduleSubscribePayload;
            _caregiversSubscribePayload = caregiversSubscribePayload;
            _homeHealthAgenciesSubscribePayload = homeHealthAgenciesSubscribePayload;
            _homeHealthAgenciesBranchListSubscribePayload = homeHealthAgenciesBranchListSubscribePayload;
            _paymentSourcesSubscribePayload = paymentSourcesSubscribePayload;
            _paymentSourceBranchesSubscribePayload = paymentSourceBranchesSubscribePayload;
            _payerServicesSubscribePayload = payerServicesSubscribePayload;
            _evvconfigSubscribePayload = evvconfigSubscribePayload;
            _evvvendormasterSubscribePayload = evvvendormasterSubscribePayload;
            _evvvendorversionmasterSubscribePayload = evvvendorversionmasterSubscribePayload;
            _clientadditionalsubscibePayload = clientadditionalsubscibePayload;
            _clientadditional2subscibePayload = clientAdditionalDetails2SubscribePayload;
            _ClientEmergencyContactSubscribePayload = ClientEmergencyContactSubscribePayload;
            _Hconfigurations2SubscribePayload = Hconfigurations2SubscribePayload;
            _cM2K_ClientsSubscribePayload = cM2K_ClientsSubscribePayload;
            _clientTreatmentAddressSubscribePayload = clientTreatmentAddressSubscribePayload;
            _ClientPaymentSourcesSubscribePayload = ClientPaymentSourcesSubscribePayload;
            _scheduleSplitSubscribePayload = scheduleSplitSubscribePayload;
            _authorizationSubscribePayload = authorizationSubscribePayload;
            _cDSAuthSubscribePayload = cDSAuthSubscribePayload;
            _scheduleEvvExceptionsSubscribePayload = scheduleEvvExceptionsSubscribePayload;
            _scheduleEvvReasonsSubscribePayload = scheduleEvvReasonsSubscribePayload;

            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.scheduleCDInfo, _scheduleSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.Client, _clientSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.ClientadditionalDeatils, _clientadditionalsubscibePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.ClientadditionalDeatils2, _clientadditional2subscibePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.EVVVendorVersionConfigInfo, _evvvendorversionmasterSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.Caregivers, _caregiversSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.HomeHealthAgencies, _homeHealthAgenciesSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.HomeHealthAgenciesBranchList, _homeHealthAgenciesBranchListSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.PayerInfo, _paymentSourcesSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.PayerBranchInfo, _paymentSourceBranchesSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.PayerServiceInfo, _payerServicesSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.EVVConfigInfo, _evvconfigSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.EVVVendorConfigInfo, _evvvendormasterSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.ClientEmergencyContact, _ClientEmergencyContactSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.HConfigurations2, _Hconfigurations2SubscribePayload); 
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.Cm2k_Client, _cM2K_ClientsSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.ClientTreatmentAddress, _clientTreatmentAddressSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.ClientPayer, _ClientPaymentSourcesSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.scheduleSplitCDInfo, _scheduleSplitSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.scheduleAuthCDInfo, _authorizationSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.scheduleCDSAuthCDInfo, _cDSAuthSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.scheduleEvvExceptionCDInfo, _scheduleEvvExceptionsSubscribePayload);
            _modifiedDataTypeContextfactories.Add(ModifiedDataTypeContext.scheduleEvvReasonCDInfo, _scheduleEvvReasonsSubscribePayload);

        }

        public ISubscribe Create(ModifiedDataTypeContext typeContext) => _modifiedDataTypeContextfactories[typeContext];

    }
}
