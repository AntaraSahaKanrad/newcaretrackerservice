﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CareTrackerService.Susbscriber.Services.Contracts
{
    public interface ISubscribe
    {
        Task<bool> GetSubscribedPayload(dynamic @event = null);
    }
}
