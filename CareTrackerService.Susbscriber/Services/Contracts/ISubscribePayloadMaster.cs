﻿using CareTrackerService.Susbscriber.EnumValues;
using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.Services.Contracts
{
   public interface ISubscribePayloadMaster
    {
        ISubscribe Create(ModifiedDataTypeContext typeContext);
    }
}
