﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;


namespace CareTrackerService.Susbscriber.Services
{
    public class ClientTreatmentAddressSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<ClientTreatmentAddressEntity> _commonRepository;

        public ClientTreatmentAddressSubscribePayload(IMapper mapper, ICommonRepository<ClientTreatmentAddressEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }

        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {

                    var clientTreatmentAddressPayload = JsonConvert.SerializeObject(@event.Payload);
                    var clientTreatmentAddress = JsonConvert.DeserializeObject<IEnumerable<ClientTreatmentAddress>>(clientTreatmentAddressPayload);

                    List<ClientTreatmentAddress> clientTreatmentAddressList = clientTreatmentAddress;

                    var clientTreatmentAddressEntity = _mapper.Map<IEnumerable<ClientTreatmentAddressEntity>>(clientTreatmentAddressList);

                    _commonRepository.CreateCollectionData(clientTreatmentAddressEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
