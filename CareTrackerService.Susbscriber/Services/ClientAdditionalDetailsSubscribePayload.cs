﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
    public class ClientAdditionalDetailsSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<ClientAdditionalDetailsEntity> _commonRepository;
        public ClientAdditionalDetailsSubscribePayload(IMapper mapper, ICommonRepository<ClientAdditionalDetailsEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {
                    var clientadditionaldetailsPayload = JsonConvert.SerializeObject(@event.Payload);
                    var clientadditionaldetails = JsonConvert.DeserializeObject<IEnumerable<ClientAdditionalDetails>>(clientadditionaldetailsPayload);

                    var clientadditionaldetailsEntity = _mapper.Map<IEnumerable<ClientAdditionalDetailsEntity>>(clientadditionaldetails);

                    _commonRepository.CreateCollectionData(clientadditionaldetailsEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
