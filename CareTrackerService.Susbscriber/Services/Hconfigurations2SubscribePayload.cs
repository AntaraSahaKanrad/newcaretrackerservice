﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;


namespace CareTrackerService.Susbscriber.Services
{
    public class Hconfigurations2SubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<Hconfigurations2Entity> _commonRepository;
        public Hconfigurations2SubscribePayload(IMapper mapper, ICommonRepository<Hconfigurations2Entity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {

                    var Hconfigurations2Payload = JsonConvert.SerializeObject(@event.Payload);
                    var Hconfigurations2 = JsonConvert.DeserializeObject<IEnumerable<Hconfigurations2>>(Hconfigurations2Payload);

                    var Hconfigurations2Entity = _mapper.Map<IEnumerable<Hconfigurations2Entity>>(Hconfigurations2);

                    _commonRepository.CreateCollectionData(Hconfigurations2Entity);

                    return true;
                });
            }
            catch (Exception ex)
            {

                return false;
            }

        }
    }
}
