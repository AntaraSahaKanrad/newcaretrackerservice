﻿﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
    public class ClientEmergencyContactSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<ClientEmergencyContactEntity> _commonRepository;
        public ClientEmergencyContactSubscribePayload(IMapper mapper, ICommonRepository<ClientEmergencyContactEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {
                    var ClientEmergencyContactPayload = JsonConvert.SerializeObject(@event.Payload);
                    var ClientEmergencyContact = JsonConvert.DeserializeObject<IEnumerable<ClientEmergencyContact>>(ClientEmergencyContactPayload);

                    var ClientEmergencyContactEntity = _mapper.Map<IEnumerable<ClientEmergencyContactEntity>>(ClientEmergencyContact);

                    _commonRepository.CreateCollectionData(ClientEmergencyContactEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
