using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic; 
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts; 
using Microsoft.Extensions.Logging;

namespace CareTrackerService.Susbscriber.Services
{
    public class CaregiversSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<CaregiversEntity> _commonRepository;
        private readonly ILogger<CaregiversSubscribePayload> _logger;
        public CaregiversSubscribePayload(IMapper mapper, ICommonRepository<CaregiversEntity> commonRepository, ILogger<CaregiversSubscribePayload> logger)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
            _logger = logger;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {

                return await Task.Run(() =>
                {
                    var caregiversPayload = JsonConvert.SerializeObject(@event.Payload);

                    var caregivers = JsonConvert.DeserializeObject<IEnumerable<Caregivers>>(caregiversPayload);

                    string details = Convert.ToString(caregivers);                    

                    List<Caregivers> caregiversList = caregivers;

                    var caregiversEntity = _mapper.Map<IEnumerable<CaregiversEntity>>(caregiversList);

                    _commonRepository.CreateCollectionData(caregiversEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError("CaregiversSubscribePayload-GetSubscribedPayload:" + ex.ToString());

                return false;
            }
        }
    }
}