﻿using AutoMapper;
using Caretrackerservice.Core.Entity;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using CareTrackerService.Susbscriber.Models;
using CareTrackerService.Susbscriber.Services.Contracts;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CareTrackerService.Susbscriber.Services
{
    public class ScheduleEvvExceptionsSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<CaregiverTaskEvvExceptionsEntity> _commonRepository;
        private readonly ILogger<ScheduleEvvExceptionsSubscribePayload> _logger;
        public ScheduleEvvExceptionsSubscribePayload(IMapper mapper, ICommonRepository<CaregiverTaskEvvExceptionsEntity> commonRepository, ILogger<ScheduleEvvExceptionsSubscribePayload> logger)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
            _logger = logger;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {
            if (@event == null || @event.Payload == null)
                return false;

            try
            {
                return await Task.Run(() =>
                {

                    var CaregiverTaskEvvExceptionsPayload = JsonConvert.SerializeObject(@event.Payload);
                    var caregiverTaskEvvExceptions = JsonConvert.DeserializeObject<IEnumerable<CaregiverTaskEvvExceptions>>(CaregiverTaskEvvExceptionsPayload);

                    List<CaregiverTaskEvvExceptions> caregiverTaskEvvExceptionsList = caregiverTaskEvvExceptions;

                    var caregiverTaskEvvExceptionsEntity = _mapper.Map<IEnumerable<CaregiverTaskEvvExceptionsEntity>>(caregiverTaskEvvExceptionsList);

                    _commonRepository.CreateCollectionData(caregiverTaskEvvExceptionsEntity);


                    return true;
                });

            }
            catch (Exception ex)
            {
                _logger.LogError("ScheduleEvvExceptionsSubscribePayload- GetSubscribedPayload : " + ex.ToString());
                return false;
            }
        }
    }
}
