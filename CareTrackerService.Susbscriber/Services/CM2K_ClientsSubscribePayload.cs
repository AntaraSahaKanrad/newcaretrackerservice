﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
    public class CM2K_ClientsSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<CM2K_ClientsEntity> _commonRepository;
        public CM2K_ClientsSubscribePayload(IMapper mapper, ICommonRepository<CM2K_ClientsEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }

        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {
                    var CM2K_ClientsPayload = JsonConvert.SerializeObject(@event.Payload);
                    var CM2K_Clients = JsonConvert.DeserializeObject<IEnumerable<CM2K_ClientsEntity>>(CM2K_ClientsPayload);

                    List<CM2K_ClientsEntity> cM2K_Clientslist = CM2K_Clients;

                    var cM2K_ClientsEntity = _mapper.Map<IEnumerable<CM2K_ClientsEntity>>(cM2K_Clientslist);

                    _commonRepository.CreateCollectionData(cM2K_Clientslist);

                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
