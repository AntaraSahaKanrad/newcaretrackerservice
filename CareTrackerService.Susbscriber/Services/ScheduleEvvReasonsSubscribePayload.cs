﻿using AutoMapper;
using Caretrackerservice.Core.Entity;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using CareTrackerService.Susbscriber.Models;
using CareTrackerService.Susbscriber.Services.Contracts;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CareTrackerService.Susbscriber.Services
{
    public class ScheduleEvvReasonsSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<CaregiverTaskEvvReasonsEntity> _commonRepository;
        private readonly ILogger<ScheduleEvvReasonsSubscribePayload> _logger;
        public ScheduleEvvReasonsSubscribePayload(IMapper mapper, ICommonRepository<CaregiverTaskEvvReasonsEntity> commonRepository, ILogger<ScheduleEvvReasonsSubscribePayload> logger)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
            _logger = logger;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {
            if (@event == null || @event.Payload == null)
                return false;

            try
            {
                return await Task.Run(() =>
                {

                    var ScheduleEVVReasonsPayload = JsonConvert.SerializeObject(@event.Payload);
                    var ScheduleEVVReasons = JsonConvert.DeserializeObject<IEnumerable<CaregiverTaskEvvReasons>>(ScheduleEVVReasonsPayload);

                    List<CaregiverTaskEvvReasons> ScheduleEVVReasonsList = ScheduleEVVReasons;

                    var ScheduleEVVReasonsListEntity = _mapper.Map<IEnumerable<CaregiverTaskEvvReasonsEntity>>(ScheduleEVVReasonsList);

                    _commonRepository.CreateCollectionData(ScheduleEVVReasonsListEntity);


                    return true;
                });

            }
            catch (Exception ex)
            {
                _logger.LogError("ScheduleEvvReasonsSubscribePayload- GetSubscribedPayload : " + ex.ToString());
                return false;
            }
        }
    }
}
