﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
    public class HomeHealthAgenciesBranchListSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<HomeHealthAgenciesBranchListEntity> _commonRepository;

        public HomeHealthAgenciesBranchListSubscribePayload(IMapper mapper, ICommonRepository<HomeHealthAgenciesBranchListEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }

        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {

                    var homeHealthAgenciesBranchListsPayload = JsonConvert.SerializeObject(@event.Payload);
                    var homeHealthAgenciesBranchLists = JsonConvert.DeserializeObject<IEnumerable<HomeHealthAgenciesBranchList>>(homeHealthAgenciesBranchListsPayload);

                    List<HomeHealthAgenciesBranchList> homeHealthAgenciesBranchListsList = homeHealthAgenciesBranchLists;

                    var homeHealthAgenciesBranchListEntity = _mapper.Map<IEnumerable<HomeHealthAgenciesBranchListEntity>>(homeHealthAgenciesBranchListsList);

                    _commonRepository.CreateCollectionData(homeHealthAgenciesBranchListEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {

                return false; 
            }

        }
    }
}
