﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
    public class EvvVendorMasterSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<EvvVendorMasterEntity> _commonRepository;
        public EvvVendorMasterSubscribePayload(IMapper mapper, ICommonRepository<EvvVendorMasterEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }

        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {
                    var EvvvendormasterPayload = JsonConvert.SerializeObject(@event.Payload);
                    var Evvvendormaster = JsonConvert.DeserializeObject<IEnumerable<EvvVendorMasterEntity>>(EvvvendormasterPayload);

                    List<EvvVendorMasterEntity> evvvednormasterlist = Evvvendormaster;

                    var evvvendormasterEntity = _mapper.Map<IEnumerable<EvvVendorMasterEntity>>(evvvednormasterlist);

                    _commonRepository.CreateCollectionData(evvvednormasterlist);

                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
