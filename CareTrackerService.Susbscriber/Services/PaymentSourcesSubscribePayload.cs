﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using Microsoft.Extensions.Logging;

namespace CareTrackerService.Susbscriber.Services
{
    public class PaymentSourcesSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<PaymentSourcesEntity> _commonRepository;
        private readonly ILogger<ScheduleSplitSubscribePayload> _logger;
        public PaymentSourcesSubscribePayload(IMapper mapper, ICommonRepository<PaymentSourcesEntity> commonRepository, ILogger<ScheduleSplitSubscribePayload> logger)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
            _logger = logger;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {
            if (@event == null || @event.Payload == null)
                return false;

            try
            {
                return await Task.Run(() =>
                {

                    var paymentSourcesPayload = JsonConvert.SerializeObject(@event.Payload);
                    var paymentSources = JsonConvert.DeserializeObject<IEnumerable<PaymentSources>>(paymentSourcesPayload);

                    List<PaymentSources> paymentSourcesList = paymentSources;

                    var paymentSourcesEntity = _mapper.Map<IEnumerable<PaymentSourcesEntity>>(paymentSourcesList);

                    _commonRepository.CreateCollectionData(paymentSourcesEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(" PaymentSourcesSubscribePayload - GetSubscribedPayload : " + ex.ToString());
                return false;
            }
        }
    }
}
