﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
    public class ClientSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<ClientsEntity> _commonRepository;
        public ClientSubscribePayload(IMapper mapper, ICommonRepository<ClientsEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {

                    var clientsPayload = JsonConvert.SerializeObject(@event.Payload);
                    var clients = JsonConvert.DeserializeObject<IEnumerable<Clients>>(clientsPayload);

                    var clientsEntity = _mapper.Map<IEnumerable<ClientsEntity>>(clients);

                    _commonRepository.CreateCollectionData(clientsEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
