﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
    public class PayerServicesSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<PayerServicesEntity> _commonRepository;

        public PayerServicesSubscribePayload(IMapper mapper, ICommonRepository<PayerServicesEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {
            if (@event == null || @event.Payload == null)
                return false;

            try
            {
                return await Task.Run(() =>
                {

                    var payerServicesPayload = JsonConvert.SerializeObject(@event.Payload);
                    var payerServices = JsonConvert.DeserializeObject<IEnumerable<PayerServices>>(payerServicesPayload);

                    List<PayerServices> payerServicesList = payerServices;

                    var payerServicesEntity = _mapper.Map<IEnumerable<PayerServicesEntity>>(payerServicesList);

                    _commonRepository.CreateCollectionData(payerServicesEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
