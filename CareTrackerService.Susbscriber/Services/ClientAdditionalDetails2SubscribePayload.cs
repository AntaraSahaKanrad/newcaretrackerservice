﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
namespace CareTrackerService.Susbscriber.Services
{
    public class ClientAdditionalDetails2SubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<ClientAdditionalDetails2Entity> _commonRepository;
        public ClientAdditionalDetails2SubscribePayload(IMapper mapper, ICommonRepository<ClientAdditionalDetails2Entity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                { 
                        var clientadditionaldetails2Payload = JsonConvert.SerializeObject(@event.Payload);
                        var clientAdditionaldeatils2 = JsonConvert.DeserializeObject<IEnumerable<ClientAdditionalDetails2>>(clientadditionaldetails2Payload);

                        var clientadditionaldetailsEntity = _mapper.Map<IEnumerable<ClientAdditionalDetails2Entity>>(clientAdditionaldeatils2);

                        _commonRepository.CreateCollectionData(clientadditionaldetailsEntity);
                     
                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
