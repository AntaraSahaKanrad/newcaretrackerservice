﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
   public class EvvVendorVersionasterSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<EvvVendorVersionMasterEntity> _commonRepository;
        public EvvVendorVersionasterSubscribePayload(IMapper mapper, ICommonRepository<EvvVendorVersionMasterEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }

        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {

                    var EvvvendorversionmasterPayload = JsonConvert.SerializeObject(@event.Payload);
                    var Evvvendorversionmaster = JsonConvert.DeserializeObject<IEnumerable<EvvVendorVersionMasterEntity>>(EvvvendorversionmasterPayload);

                    List<EvvVendorVersionMasterEntity> evvvendorversionmasterlist = Evvvendorversionmaster;

                    var evvvendorversionmasterEntity = _mapper.Map<IEnumerable<EvvVendorVersionMasterEntity>>(evvvendorversionmasterlist);

                    _commonRepository.CreateCollectionData(evvvendorversionmasterlist);

                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
