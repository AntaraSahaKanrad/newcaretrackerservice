﻿using AutoMapper;
using Caretrackerservice.Core.Entity;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using CareTrackerService.Susbscriber.Models;
using CareTrackerService.Susbscriber.Services.Contracts;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks; 

namespace CareTrackerService.Susbscriber.Services
{
    public class CDSAuthSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<CDSPlanYearServicesEntity> _commonRepository;
        private readonly ILogger<CDSAuthSubscribePayload> _logger;

        public CDSAuthSubscribePayload(IMapper mapper, ICommonRepository<CDSPlanYearServicesEntity> commonRepository, ILogger<CDSAuthSubscribePayload> logger)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
            _logger = logger;
        }
        public  async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {
            if (@event == null || @event.Payload == null)
                return false;

            try
            {
                return await Task.Run(() =>
                {

                    var CDSPlanYearServicesPayload = JsonConvert.SerializeObject(@event.Payload);
                    var CDSPlanYearServices = JsonConvert.DeserializeObject<IEnumerable<CDSPlanYearServices>>(CDSPlanYearServicesPayload);

                    List<CDSPlanYearServices> CDSPlanYearServicesList = CDSPlanYearServices;

                    var _CDSPlanYearServicesEntity = _mapper.Map<IEnumerable<CDSPlanYearServicesEntity>>(CDSPlanYearServicesList);

                    _commonRepository.CreateCollectionData(_CDSPlanYearServicesEntity);


                    return true;
                });

            }
            catch (Exception ex)
            {
                _logger.LogError("CDSAuthSubscribePayload- GetSubscribedPayload : " + ex.ToString());
                return false;
            }
        }
    }
}
