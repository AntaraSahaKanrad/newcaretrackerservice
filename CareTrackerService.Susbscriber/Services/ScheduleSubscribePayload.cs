﻿using Caretrackerservice.Core.Entity;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using Newtonsoft.Json;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace CareTrackerService.Susbscriber.Services
{
    public class ScheduleSubscribePayload : ISubscribe
    {
        //private readonly IVisitsRepository _visitsRepository;
        private readonly IMapper _mapper;
        private readonly ICommonRepository<CaregiverTaskEntity> _commonRepository;
        private readonly ILogger<ScheduleSubscribePayload> _logger;

        public ScheduleSubscribePayload(IMapper mapper, ICommonRepository<CaregiverTaskEntity> commonRepository, ILogger<ScheduleSubscribePayload> logger)
        {
            //_visitsRepository = visitsRepository;
            _mapper = mapper;
            _commonRepository = commonRepository;
            _logger = logger;

        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {
            if (@event == null || @event.Payload == null)
                return false;

            try
            {
                return await Task.Run(() =>
                {

                    var caregiverTasksPayload = JsonConvert.SerializeObject(@event.Payload);
                    var caregiverTasks = JsonConvert.DeserializeObject<List<CaregiverTasks>>(caregiverTasksPayload);

                    List<CaregiverTasks> cgtaskentity = caregiverTasks;

                    var caregivertaskentity = _mapper.Map<IEnumerable<CaregiverTaskEntity>>(cgtaskentity);

                    _commonRepository.CreateCollectionData(caregivertaskentity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError("ScheduleSplitSubscribePayload- GetSubscribedPayload : " + ex.ToString());
                return false;
            }
        }
    }
}
