﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;

namespace CareTrackerService.Susbscriber.Services
{
    public class PaymentSourceBranchesSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<PaymentSourceBranchesEntity> _commonRepository;

        public PaymentSourceBranchesSubscribePayload(IMapper mapper, ICommonRepository<PaymentSourceBranchesEntity> commonRepository)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {
            if (@event == null || @event.Payload == null)
                return false;

            try
            {
                return await Task.Run(() =>
                {

                    var paymentSourceBranchesPayload = JsonConvert.SerializeObject(@event.Payload);
                    var paymentSourceBranches = JsonConvert.DeserializeObject<IEnumerable<PaymentSourceBranches>>(paymentSourceBranchesPayload);

                    List<PaymentSourceBranches> paymentSourceBranchsList = paymentSourceBranches;

                    var paymentSourceBranchesEntity = _mapper.Map<IEnumerable<PaymentSourceBranchesEntity>>(paymentSourceBranchsList);

                    _commonRepository.CreateCollectionData(paymentSourceBranchesEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
