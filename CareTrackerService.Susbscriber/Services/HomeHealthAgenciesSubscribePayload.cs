﻿using AutoMapper;
using Caretrackerservice.Services.Visits.Service.Contracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Caretrackerservice.Core.Entity;
using Newtonsoft.Json;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using Microsoft.Extensions.Logging;

namespace CareTrackerService.Susbscriber.Services
{
    public class HomeHealthAgenciesSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<HomeHealthAgenciesEntity> _commonRepository;
        private readonly ILogger<HomeHealthAgenciesSubscribePayload> _logger;
        public HomeHealthAgenciesSubscribePayload(IMapper mapper, ICommonRepository<HomeHealthAgenciesEntity> commonRepository, ILogger<HomeHealthAgenciesSubscribePayload> logger)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
            _logger = logger;
        }

        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {

            if (@event == null || @event.Payload == null)
                return false;
            try
            {
                return await Task.Run(() =>
                {

                    var homeHealthAgenciesPayload = JsonConvert.SerializeObject(@event.Payload);
                    var homeHealthAgencies = JsonConvert.DeserializeObject<IEnumerable<HomeHealthAgencies>>(homeHealthAgenciesPayload);
                    string details = Convert.ToString(homeHealthAgencies);
                    _logger.LogInformation($"Created caregivers {details}");


                    List<HomeHealthAgencies> homeHealthAgenciesList = homeHealthAgencies;

                    var homeHealthAgenciesEntity = _mapper.Map<IEnumerable<HomeHealthAgenciesEntity>>(homeHealthAgenciesList);

                    _commonRepository.CreateCollectionData(homeHealthAgenciesEntity);

                    return true;
                });
            }
            catch (Exception ex)
            {
                _logger.LogError("HomeHealthAgenciesSubscribePayload -GetSubscribedPayload : " + ex.ToString());
                return false;
            }

        }
    }
}
