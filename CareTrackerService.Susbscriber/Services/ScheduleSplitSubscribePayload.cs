﻿using Caretrackerservice.Core.Entity;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using CareTrackerService.Susbscriber.Services.Contracts;
using Newtonsoft.Json;
using CareTrackerService.Susbscriber.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;

namespace CareTrackerService.Susbscriber.Services
{
    public class ScheduleSplitSubscribePayload : ISubscribe
    {
        private readonly IMapper _mapper;
        private readonly ICommonRepository<CaregiverTaskChildSchedulesEntity> _commonRepository;
        private readonly ILogger<ScheduleSplitSubscribePayload> _logger;
        public ScheduleSplitSubscribePayload(IMapper mapper, ICommonRepository<CaregiverTaskChildSchedulesEntity> commonRepository, ILogger<ScheduleSplitSubscribePayload> logger)
        {
            _mapper = mapper;
            _commonRepository = commonRepository;
            _logger = logger;
        }
        public async Task<bool> GetSubscribedPayload(dynamic @event = null)
        {
            if (@event == null || @event.Payload == null)
                return false;

            try
            {
                return await Task.Run(() =>
                {

                    var ChildschedulesPayload = JsonConvert.SerializeObject(@event.Payload);
                    var Childschedules = JsonConvert.DeserializeObject<IEnumerable<CaregiverTaskChildSchedules>>(ChildschedulesPayload);

                    List<CaregiverTaskChildSchedules> paymentSourcesList = Childschedules;

                    var ChildschedulesEntity = _mapper.Map<IEnumerable<CaregiverTaskChildSchedulesEntity>>(paymentSourcesList);

                    _commonRepository.CreateCollectionData(ChildschedulesEntity);


                    return true;
                });

            }
            catch (Exception ex)
            {
                _logger.LogError("ScheduleSplitSubscribePayload- GetSubscribedPayload : " + ex.ToString());
                return false;
            }
        }
    }
}
