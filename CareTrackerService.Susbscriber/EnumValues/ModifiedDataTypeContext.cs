﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CareTrackerService.Susbscriber.EnumValues
{
  public  enum ModifiedDataTypeContext
    {
        scheduleCDInfo,
        Client,
        ClientadditionalDeatils,
        ClientadditionalDeatils2,
        Caregivers,
        HomeHealthAgencies,
        HomeHealthAgenciesBranchList,
        PayerInfo,
        PayerBranchInfo,
        PayerServiceInfo,
        EVVConfigInfo,
        EVVVendorConfigInfo,
        EVVVendorVersionConfigInfo,
        ClientEmergencyContact,
        HConfigurations2,       
        Cm2k_Client,
        ClientTreatmentAddress,
        ClientPayer,
        scheduleSplitCDInfo,
        scheduleAuthCDInfo,
        scheduleCDSAuthCDInfo,
        scheduleEvvReasonCDInfo,
        scheduleEvvExceptionCDInfo
    }
}
