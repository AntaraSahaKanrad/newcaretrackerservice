﻿using Caretrackerservice.Services.Visits.Service.Contracts;
using Caretrackerservice.Services.Visits.Service;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Caretrackerservice.Services.Common.Behaviors;
using System.Reflection;
using AutoMapper;
using Microsoft.Extensions;

namespace Caretrackerservice.Services
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServicesServiceCollection(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpContextAccessor();
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(HttpContextMiddleware<,>));
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            services.AddTransient<IVisit, Visit>();
            return services;
        }
    }
}
