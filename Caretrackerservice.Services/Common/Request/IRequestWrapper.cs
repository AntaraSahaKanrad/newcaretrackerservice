﻿using Caretrackerservice.Services.Common.Response;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Services.Common.Request
{
    public interface IRequestWrapper<T> : IRequest<Response<T>>
    {
    }

    public interface IHandlerWrapper<TIn, Tout> : IRequestHandler<TIn, Response<Tout>>
        where TIn : IRequestWrapper<Tout>
    {

    }
}
