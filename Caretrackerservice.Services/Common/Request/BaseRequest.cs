﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Services.Common.Request
{
    public class BaseRequest
    {
        public int UserId { get; set; }
        public int HHA { get; set; }
    }
}
