﻿using Caretrackerservice.Services.Common.Request;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Caretrackerservice.Services.Common.Behaviors
{
    public class HttpContextMiddleware<TIn, TOut> : IPipelineBehavior<TIn, TOut>    
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public HttpContextMiddleware(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public async Task<TOut> Handle(TIn request, CancellationToken cancellationToken, RequestHandlerDelegate<TOut> next)
        {
            if (_httpContextAccessor.HttpContext.User == null)
            {
                if (request is BaseRequest br)
                {
                    br.UserId = 4080;
                    br.HHA = 247;
                }
            }
            else
            {
                /*var hhaClaim = _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == "hha").FirstOrDefault().Value;
                var userIdClaim = _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == "userId").FirstOrDefault().Value;



                if (request is BaseRequest br)
                {
                    br.UserId = Convert.ToInt32(userIdClaim);
                    br.HHA = Convert.ToInt32(hhaClaim);
                }*/
            }
            var result = await next();

            return result;
        }
    }
}
