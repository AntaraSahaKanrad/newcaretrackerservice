﻿using Caretrackerservice.Services.Visits.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Caretrackerservice.Services.Visits.Service.Contracts
{
    public interface IVisit
    {
        Task<IEnumerable<VisitsDto>> GetVisits();
    }
}
