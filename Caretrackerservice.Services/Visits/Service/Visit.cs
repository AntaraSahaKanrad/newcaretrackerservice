﻿using AutoMapper;
using Caretrackerservice.Services.Infrastructure.DataRepositoryContracts;
using Caretrackerservice.Services.Visits.DTO;
using Caretrackerservice.Services.Visits.Service.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Caretrackerservice.Services.Visits.Service
{
    public class Visit : IVisit
    {
        IVisitsRepository _visitsRepository;
        IMapper _mapper;
        public Visit(IVisitsRepository visitsRepository, IMapper mapper)
        {
            _visitsRepository = visitsRepository;
            _mapper = mapper;
        }
        public async Task<IEnumerable<VisitsDto>> GetVisits()
        {
            var Visitsresult=await _visitsRepository.GetVisits();
             
            var visits = _mapper.Map<IEnumerable<VisitsDto>>(Visitsresult);
            return visits;
        }

       
    }
}
