﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Services.Visits.DTO
{
    public class VisitsDto
    {

        public ObjectId Id { get; set; }
        public Basics basics { get; set; }
    }
    public class Basics
    {
        public DateTime createdOn { get; set; }

        public int CreatedBy { get; set; }
        public DateTime LastModifiedOn { get; set; }
        public string CreatedByUserName { get; set; }
        public string ApplicationCode { get; set; }// v(10) - HH, HP, PD, PHC, 11, 12, 13
        public int HHA { get; set; }
        public Int64 ScheduleAppID { get; set; }
    }

    public class Client
    {
        public Int64 ClientUID { get; set; }
        public int ClientAppID { get; set; }
        public string ClientBranchName { get; set; }
        public int ClientBranchID { get; set; }
        public string ClientDisplayName { get; set; }
        public string ClientStatus { get; set; }
    }
}
   