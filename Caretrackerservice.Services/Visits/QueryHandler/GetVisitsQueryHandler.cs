﻿using Caretrackerservice.Services.Common.Request;
using Caretrackerservice.Services.Common.Response;
using Caretrackerservice.Services.Visits.DTO;
using Caretrackerservice.Services.Visits.Query;
using Caretrackerservice.Services.Visits.Service.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Caretrackerservice.Services.Visits.QueryHandler
{
    public class GetVisitsQueryHandler : IHandlerWrapper<GetVisitsQuery, IEnumerable<VisitsDto>>
    {
        private readonly IVisit _visit;
        public GetVisitsQueryHandler(IVisit visit)
        {
            _visit = visit;
        }
        public async Task<Response<IEnumerable<VisitsDto>>> Handle(GetVisitsQuery request, CancellationToken cancellationToken)
        {

            var visitsDtos = await _visit.GetVisits();
            return Response.Ok<IEnumerable<VisitsDto>>(visitsDtos, "");
        }        
    }
}
