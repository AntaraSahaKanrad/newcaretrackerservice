﻿using Caretrackerservice.Services.Common.Request;
using Caretrackerservice.Services.Visits.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Services.Visits.Query
{
   public class GetVisitsQuery : BaseRequest, IRequestWrapper<IEnumerable<VisitsDto>>
    {
    }
}
