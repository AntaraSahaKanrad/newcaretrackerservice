﻿using AutoMapper;
using Caretrackerservice.Core.Entity;
using Caretrackerservice.Services.Visits.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Caretrackerservice.Services.Visits.MappingProfiles
{
    public class VisitsMappingProfile:Profile
    {
        public VisitsMappingProfile()
        {
            CreateMap<VisitsEntity, VisitsDto>();
            CreateMap<VisitsBasicsEntity, Basics>();

        }
    }
}
