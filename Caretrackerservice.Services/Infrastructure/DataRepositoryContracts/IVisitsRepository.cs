﻿using Caretrackerservice.Core.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Caretrackerservice.Services.Infrastructure.DataRepositoryContracts
{
    public interface IVisitsRepository  
    {
        Task<IEnumerable<VisitsEntity>> GetVisits();
        void CreateSingleVisit(VisitsEntity visitsEntity);
        void CreateVisits(IEnumerable<VisitsEntity> visitsEntity);
        void ModifySingleVisit(VisitsEntity visitsEntity);
        void ModifyVisits(IEnumerable<VisitsEntity> visitsEntity);

        void UpdateSingleVisitBasic(VisitsBasicsEntity visitsBasicsEntity, int cgtaskid);
         
    }
}
