﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Caretrackerservice.Services.Infrastructure.DataRepositoryContracts
{
    public interface ICommonRepository<T>
    {
        IEnumerable<T> GetCollectionData();
        void CreateCollectionData(IEnumerable<T> collectionData);
        void DeleteCollectionData(Expression<Func<T, bool>> filterExpression);
        void DeleteManyCollectionData(IEnumerable<T> collectionData);

    }
}
